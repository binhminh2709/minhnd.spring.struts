package dao;

import model.Customer;

import java.util.List;

public interface CustomerDao {

    void addCustomer(Customer customer);

    List findAllCustomer();

}